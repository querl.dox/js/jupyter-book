#!/bin/bash

git clone https://gitlab.com/querl.dox/js/backbone/skeleton backbone/skeleton
mkdir backbone/source
wget -P backbone/source https://code.jquery.com/jquery-3.4.1.js
wget -P backbone/source http://underscorejs.org/underscore.js
wget -P backbone/source https://backbonejs.org/backbone.js
