#!/bin/bash

git clone https://gitlab.com/querl.dox/js/angular/tour-of-heroes angular/tour-of-heroes

cd angular/tour-of-heroes

git checkout 0.1.0

npm install

ng build --base-href ./ --progress --ts-config tsconfig.jupyter.json --verbose
