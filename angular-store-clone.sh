#!/bin/bash

git clone https://gitlab.com/querl.dox/js/angular/my-store angular/my-store

cd angular/my-store

git checkout 0.5.2

npm install

ng build --base-href ./ --progress --ts-config tsconfig.jupyter.json --verbose